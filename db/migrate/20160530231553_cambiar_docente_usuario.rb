class CambiarDocenteUsuario < ActiveRecord::Migration
  def change
    rename_column :actividads, :docente_id, :usuario_id
  end
end
