class CreateActividads < ActiveRecord::Migration
  def change
    create_table :actividads do |t|
      t.integer :tipo_id
      t.integer :actividad_id
      t.integer :docente_id
      t.string :nombre
      t.string :titulo
      t.string :autores
      t.string :presentador
      t.string :participantes
      t.date :fecha
      t.string :lugar
      t.string :origen_financiamiento
      t.text :comentario

      t.timestamps
    end
  end
end
