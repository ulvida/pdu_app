class CreateSedes < ActiveRecord::Migration
  def change
    create_table :sedes do |t|
      t.string :nombre
      t.string :web
      t.integer :cenur_id
      t.text :comentario

      t.timestamps
    end
  end
end
