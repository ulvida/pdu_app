class AddAdminToUsuarios < ActiveRecord::Migration
  def change
    add_column :usuarios, :superadmin, :boolean, default: false
  end
end
