class CreateFacultads < ActiveRecord::Migration
  def change
    create_table :facultads do |t|
      t.string :nombre
      t.string :web
      t.text :comentario

      t.timestamps
    end
  end
end
