class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.string :nombre
      t.string :apellido
      t.string :ci
      t.integer :grado
      t.integer :sede_id
      t.integer :facultad_id
      t.integer :anio_aprobado
      t.integer :horas_servicio
      t.integer :horas_sede
      t.boolean :dt
      t.boolean :viajero
      t.integer :caracter_id
      t.string :calidad
      t.boolean :art9

      t.timestamps
    end
  end
end
