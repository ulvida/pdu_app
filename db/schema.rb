# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160530231553) do

  create_table "actividads", force: true do |t|
    t.integer  "tipo_id"
    t.integer  "actividad_id"
    t.integer  "usuario_id"
    t.string   "nombre"
    t.string   "titulo"
    t.string   "autores"
    t.string   "presentador"
    t.string   "participantes"
    t.date     "fecha"
    t.string   "lugar"
    t.string   "origen_financiamiento"
    t.text     "comentario"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "caracters", force: true do |t|
    t.string   "descripcion"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "cenurs", force: true do |t|
    t.string   "nombre"
    t.string   "web"
    t.text     "comentario"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "facultads", force: true do |t|
    t.string   "nombre"
    t.string   "web"
    t.text     "comentario"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sedes", force: true do |t|
    t.string   "nombre"
    t.string   "web"
    t.integer  "cenur_id"
    t.text     "comentario"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tipo_actividads", force: true do |t|
    t.string   "tipo"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "tipos", force: true do |t|
    t.string   "tipo"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "usuarios", force: true do |t|
    t.string   "nombre"
    t.string   "apellido"
    t.string   "ci"
    t.integer  "grado"
    t.integer  "sede_id"
    t.integer  "facultad_id"
    t.integer  "anio_aprobado"
    t.integer  "horas_servicio"
    t.integer  "horas_sede"
    t.boolean  "dt"
    t.boolean  "viajero"
    t.integer  "caracter_id"
    t.string   "calidad"
    t.boolean  "art9"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest"
    t.string   "email"
    t.boolean  "superadmin",      default: false
  end

  add_index "usuarios", ["email"], name: "index_usuarios_on_email", unique: true

end
