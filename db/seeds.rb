# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Caracter.create!(descripcion: "efectivo")
Caracter.create!(descripcion: "interino")
Caracter.create!(descripcion: "pasante")

Cenur.create!(nombre: "Litoral Norte")
Cenur.create!(nombre: "Este",
              web: "http://www.cure.edu.uy")
Cenur.create!(nombre: "Región Noreste",
              web: "http://www.noreste.udelar.edu.uy")

Sede.create!(nombre: "Salto",
             web: "http://www.unorte.edu.uy",
             cenur_id: 1)
Sede.create!(nombre: "Paysandú",
             web: "http://www.cup.edu.uy",
             cenur_id: 1)
Sede.create!(nombre: "Rocha",
             web: "http://www.cure.edu.uy",
             cenur_id: 2)
Sede.create!(nombre: "Maldonado",
             web: "http://www.cure.edu.uy",
             cenur_id: 2)
Sede.create!(nombre: "Treinta y Tres",
             web: "http://www.cure.edu.uy",
             cenur_id: 2)
Sede.create!(nombre: "Rivera",
             web: "http://www.rivera.udelar.edu.uy",
             cenur_id: 3)
Sede.create!(nombre: "Tacuarembó",
             web: "http://www.tacuarembo.udelar.edu.uy",
             cenur_id: 3)
Sede.create!(nombre: "Cerro Largo",
             web: "http://www.cerrolargo.udelar.edu.uy",
             cenur_id: 3)

Facultad.create!(nombre: "Arquitectura",
             web: "http://www.farq.edu.uy")
Facultad.create!(nombre: "Derecho",
             web: "http://www.fder.edu.uy")
Facultad.create!(nombre: "Ingeniería",
             web: "http://www.fing.edu.uy")
Facultad.create!(nombre: "Medicina",
             web: "http://www.fmed.edu.uy")
Facultad.create!(nombre: "Ciencias Sociales",
             web: "http://www.cienciassociales.edu.uy")


Usuario.create!(nombre:  "Juan",
             apellido: "Perez",
             email: "juan@unorte.edu.uy",
             password:              "foobar",
             password_confirmation: "foobar",
             ci: rand(10000000),
             grado: 1,
             sede_id: 1,
             facultad_id: 1,
             anio_aprobado: 1,
             horas_servicio: 1,
             horas_sede: 1,
             dt: false,
             viajero: false,
             caracter_id: 1,
             calidad: "MyString",
             art9: false,
             superadmin: true)

Usuario.create!(nombre:  "Daniel",
             apellido: "Viñar",
             email: "dvinar@cci.edu.uy",
             password:              "daniel",
             password_confirmation: "daniel",
             ci: rand(10000000),
             grado: 1,
             sede_id: 3,
             facultad_id: 3,
             anio_aprobado: 1,
             horas_servicio: 1,
             horas_sede: 1,
             dt: false,
             viajero: true,
             caracter_id: 2,
             calidad: "MyString",
             art9: false,
             superadmin: true)

Usuario.create!(nombre:  "Tabaré",
             apellido: "Fernandez Aguerre",
             email: "tfaguerre@cci.edu.uy",
             password:              "tabare",
             password_confirmation: "tabare",
             ci: rand(10000000),
             grado: 5,
             sede_id: 7,
             facultad_id: 5,
             anio_aprobado: 1,
             horas_servicio: 1,
             horas_sede: 1,
             dt: true,
             viajero: true,
             caracter_id: 1,
             calidad: "MyString",
             art9: false,
             superadmin: true)


99.times do |n|
  nombre  = Faker::Name.first_name
  apellido = Faker::Name.last_name
  email = "ejemplo-#{n+1}@ejemplo.org"
  password = "password"
  Usuario.create!(nombre:  nombre,
               apellido: apellido,
               email: email,
               password:              password,
               password_confirmation: password,
               ci: rand(10000000),
               grado: 1+rand(5),
               sede_id: 1+rand(8),
               facultad_id: 1+rand(5),
               anio_aprobado: 2016-rand(6),
               horas_servicio: 40,
               horas_sede: 30,
               dt: rand(2) == 1,
               viajero: rand(2) == 1,
               caracter_id: 1,
               calidad: "MyString",
               art9: rand(2) == 1)
end

## Creación de los tipos de proyectos
Tipo.create!(tipo: "Enseñanza")
Tipo.create!(tipo: "Investigación")
Tipo.create!(tipo: "Extensión")

## Creación de los tipos de actividades
TipoActividad.create!(tipo: "Congreso")
TipoActividad.create!(tipo: "Conferencia")
TipoActividad.create!(tipo: "Difusión cultural")

