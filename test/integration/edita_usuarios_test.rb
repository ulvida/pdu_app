require 'test_helper'

class EditaUsuariosTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  def setup
    @usuario = usuarios(:one)
  end

  test "unsuccessful edit" do
    log_in_as(@usuario)
    get edit_usuario_path(@usuario)
    assert_template 'usuarios/edit'
    patch usuario_path(@usuario),  usuario: {nombre: "", 
                                    email: "foo@invalid",
                                    password:              "foo",
                                    password_confirmation: "bar" }
    assert_template 'usuarios/edit'
  end

  test "successful edit with friendly forwarding" do
    get edit_usuario_path(@usuario)
    log_in_as(@usuario)
    assert_redirected_to edit_usuario_path(@usuario)
    name  = "Foo Bar"
    email = "foo@bar.com"
    patch usuario_path(@usuario), usuario: { nombre:  name,
                                    email: email,
                                    password:              "",
                                    password_confirmation: "" }
    assert_not flash.empty?
    assert_redirected_to @usuario
    @usuario.reload
    assert_equal name,  @usuario.nombre
    assert_equal email, @usuario.email
  end

end
