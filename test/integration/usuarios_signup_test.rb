require 'test_helper'

class UsuariosSignupTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

  setup do
    @usuario = usuarios(:one)
    @usuario.password = @usuario.password_confirmation = "password"
  end


  test "Información de signup inválida" do
    get signup_path
    assert_no_difference 'Usuario.count' do
      post usuarios_path usuario: { nombre: "",
                                    email: "user@invalid",
                                    password: "foo",
                                    password_confirmation: "bar" }
    end
    assert_template 'usuarios/new'
  end

  test "valid signup information" do
    get signup_path
    assert_difference 'Usuario.count', 1 do
      post_via_redirect usuarios_path, usuario: { anio_aprobado: @usuario.anio_aprobado, apellido: @usuario.apellido, art9: @usuario.art9, calidad: @usuario.calidad, caracter_id: @usuario.caracter_id, ci: @usuario.ci, dt: @usuario.dt, email: 'tres@ejemplo.com', facultad_id: @usuario.facultad_id, grado: @usuario.grado, horas_sede: @usuario.horas_sede, horas_servicio: @usuario.horas_servicio, nombre: @usuario.nombre, password: 'password', password_confirmation: 'password', sede_id: @usuario.sede_id, viajero: @usuario.viajero }
    end
    assert_template 'usuarios/show'
    assert is_logged_in?
  end
end
