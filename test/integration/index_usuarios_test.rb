require 'test_helper'

class IndexUsuariosTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end

 def setup
    @admin = usuarios(:one)
    @non_admin = usuarios(:two)
  end

    test "index including pagination" do
    log_in_as(@admin)
    get usuarios_path
    assert_template 'usuarios/index'
    assert_select 'div.pagination'
    primera_pagina_usuarios = Usuario.paginate(page: 1)
    primera_pagina_usuarios.each do |usuario|
      assert_select 'a[href=?]', usuario_path(usuario), text: usuario.nombre
      unless usuario == @admin
        assert_select 'a[href=?]', usuario_path(usuario), text: 'Borrar'
      end
    end
    assert_difference 'Usuario.count', -1 do
      delete usuario_path(@non_admin)
    end
  end

  test "index as non-admin" do
    log_in_as(@non_admin)
    get usuarios_path
    assert_select 'a', text: 'Borrar', count: 0
  end
end
