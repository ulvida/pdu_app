require 'test_helper'

class CaractersControllerTest < ActionController::TestCase
  setup do
    @caracter = caracters(:one)
    @admin = usuarios(:one)
    @non_admin = usuarios(:two)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:caracters)
  end

  test "should get new" do
    log_in_as(@admin)
    get :new
    assert_response :success
  end

  test "should create caracter" do
    log_in_as(@admin)
    assert_difference('Caracter.count') do
      post :create, caracter: { descripcion: @caracter.descripcion }
    end

    assert_redirected_to caracter_path(assigns(:caracter))
  end

  test "should show caracter" do
    get :show, id: @caracter
    assert_response :success
  end

  test "should get edit" do
    log_in_as(@admin)
    get :edit, id: @caracter
    assert_response :success
  end

  test "should update caracter" do
    log_in_as(@admin)
    patch :update, id: @caracter, caracter: { descripcion: @caracter.descripcion }
    assert_redirected_to caracter_path(assigns(:caracter))
  end

  test "should destroy caracter" do
    log_in_as(@admin)
    assert_difference('Caracter.count', -1) do
      delete :destroy, id: @caracter
    end

    assert_redirected_to caracters_path
  end
end
