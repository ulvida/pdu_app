require 'test_helper'

class ActividadsControllerTest < ActionController::TestCase
  setup do
    @actividad = actividads(:one)
    @usuario = usuarios(:one)
    @otro_usuario = usuarios(:two)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:actividads)
  end

  test "should get new" do
    log_in_as(@usuario)
    get :new
    assert_response :success
  end

  test "should create actividad" do
    log_in_as(@usuario)
    assert_difference('Actividad.count') do
      post :create, actividad: { actividad_id: @actividad.actividad_id, autores: @actividad.autores, comentario: @actividad.comentario, usuario_id: @actividad.usuario_id, fecha: @actividad.fecha, lugar: @actividad.lugar, nombre: @actividad.nombre, origen_financiamiento: @actividad.origen_financiamiento, participantes: @actividad.participantes, presentador: @actividad.presentador, tipo_id: @actividad.tipo_id, titulo: @actividad.titulo }
    end

    assert_redirected_to actividad_path(assigns(:actividad))
  end

  test "should show actividad" do
    get :show, id: @actividad
    assert_response :success
  end

  test "should get edit" do
    log_in_as(@usuario)
    get :edit, id: @actividad
    assert_response :success
  end

  test "should update actividad" do
    log_in_as(@usuario)
    patch :update, id: @actividad, actividad: { actividad_id: @actividad.actividad_id, autores: @actividad.autores, comentario: @actividad.comentario, usuario_id: @actividad.usuario_id, fecha: @actividad.fecha, lugar: @actividad.lugar, nombre: @actividad.nombre, origen_financiamiento: @actividad.origen_financiamiento, participantes: @actividad.participantes, presentador: @actividad.presentador, tipo_id: @actividad.tipo_id, titulo: @actividad.titulo }
    assert_redirected_to actividad_path(assigns(:actividad))
  end

  test "should destroy actividad" do
    log_in_as(@usuario)
    assert_difference('Actividad.count', -1) do
      delete :destroy, id: @actividad
    end

    assert_redirected_to actividads_path
  end
end
