require 'test_helper'

class CenursControllerTest < ActionController::TestCase
  setup do
    @cenur = cenurs(:one)
    @admin = usuarios(:one)
    @non_admin = usuarios(:two)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cenurs)
  end

  test "should get new" do
    log_in_as(@admin)
    get :new
    assert_response :success
  end

  test "should create cenur" do
    log_in_as(@admin)
    assert_difference('Cenur.count') do
      post :create, cenur: { comentario: @cenur.comentario, nombre: @cenur.nombre, web: @cenur.web }
    end

    assert_redirected_to cenur_path(assigns(:cenur))
  end

  test "should show cenur" do
    get :show, id: @cenur
    assert_response :success
  end

  test "should get edit" do
    log_in_as(@admin)
    get :edit, id: @cenur
    assert_response :success
  end

  test "should update cenur" do
    log_in_as(@admin)
    patch :update, id: @cenur, cenur: { comentario: @cenur.comentario, nombre: @cenur.nombre, web: @cenur.web }
    assert_redirected_to cenur_path(assigns(:cenur))
  end

  test "should destroy cenur" do
    log_in_as(@admin)
    assert_difference('Cenur.count', -1) do
      delete :destroy, id: @cenur
    end

    assert_redirected_to cenurs_path
  end
end
