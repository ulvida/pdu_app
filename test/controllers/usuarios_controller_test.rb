require 'test_helper'

class UsuariosControllerTest < ActionController::TestCase
  setup do
    @usuario = usuarios(:one)
    @otro_usuario = usuarios(:two)
  end

  test "should redirect index when not logged in" do
    get :index
    assert_redirected_to login_url
    # assert_response :success
    # assert_not_nil assigns(:usuarios)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should redirect edit when not logged in" do
    get :edit, id: @usuario
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    patch :update, id: @usuario, usuario: { nombre: @usuario.nombre, email: @usuario.email }
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect edit when logged in as wrong user" do
    log_in_as(@otro_usuario)
    get :edit, id: @usuario
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect update when logged in as wrong user" do
    log_in_as(@otro_usuario)
    patch :update, id: @usuario, usuario: { nombre: @usuario.nombre, email: @usuario.email }
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should create usuario" do
    assert_difference('Usuario.count') do
#      post :create, usuario: { anio_aprobado: @usuario.anio_aprobado, apellido: @usuario.apellido, art9: @usuario.art9, calidad: @usuario.calidad, caracter_id: @usuario.caracter_id, ci: @usuario.ci, dt: @usuario.dt, email: @usuario.email, facultad_id: @usuario.facultad_id, grado: @usuario.grado, horas_sede: @usuario.horas_sede, horas_servicio: @usuario.horas_servicio, nombre: @usuario.nombre, password_digest: @usuario.password_digest, sede_id: @usuario.sede_id, viajero: @usuario.viajero }
      post :create, usuario: { anio_aprobado: @usuario.anio_aprobado, apellido: @usuario.apellido, art9: @usuario.art9, calidad: @usuario.calidad, caracter_id: @usuario.caracter_id, ci: @usuario.ci, dt: @usuario.dt, email: 'tres@ejemplo.com', facultad_id: @usuario.facultad_id, grado: @usuario.grado, horas_sede: @usuario.horas_sede, horas_servicio: @usuario.horas_servicio, nombre: @usuario.nombre, password: 'password', password_confirmation: 'password', sede_id: @usuario.sede_id, viajero: @usuario.viajero }
    end

    assert_redirected_to usuario_path(assigns(:usuario))
  end

  test "should show usuario" do
    get :show, id: @usuario
    assert_response :success
  end

  test "should get edit" do
    log_in_as(@usuario)
    get :edit, id: @usuario
    assert_response :success
  end

  test "should update usuario" do
    log_in_as(@usuario)
    patch :update, id: @usuario, usuario: { anio_aprobado: @usuario.anio_aprobado, apellido: @usuario.apellido, art9: @usuario.art9, calidad: @usuario.calidad, caracter_id: @usuario.caracter_id, ci: @usuario.ci, dt: @usuario.dt, facultad_id: @usuario.facultad_id, grado: @usuario.grado, horas_sede: @usuario.horas_sede, horas_servicio: @usuario.horas_servicio, nombre: @usuario.nombre, sede_id: @usuario.sede_id, viajero: @usuario.viajero }
    assert_redirected_to usuario_path(assigns(:usuario))
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Usuario.count' do
      delete :destroy, id: @usuario
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when logged in as a non-admin" do
    log_in_as(@otro_usuario)
    assert_no_difference 'Usuario.count' do
      delete :destroy, id: @usuario
    end
    assert_redirected_to root_url
  end

#  test "should destroy usuario" do
#    assert_difference('Usuario.count', -1) do
#      delete :destroy, id: @usuario
#    end
#    assert_redirected_to usuarios_path
#  end
end
