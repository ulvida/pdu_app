require 'test_helper'

class FacultadsControllerTest < ActionController::TestCase
  setup do
    @facultad = facultads(:one)
    @admin = usuarios(:one)
    @non_admin = usuarios(:two)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:facultads)
  end

  test "should get new" do
    log_in_as(@admin)
    get :new
    assert_response :success
  end

  test "should create facultad" do
    log_in_as(@admin)
    assert_difference('Facultad.count') do
      post :create, facultad: { comentario: @facultad.comentario, nombre: @facultad.nombre, web: @facultad.web }
    end

    assert_redirected_to facultad_path(assigns(:facultad))
  end

  test "should show facultad" do
    get :show, id: @facultad
    assert_response :success
  end

  test "should get edit" do
    log_in_as(@admin)
    get :edit, id: @facultad
    assert_response :success
  end

  test "should update facultad" do
    log_in_as(@admin)
    patch :update, id: @facultad, facultad: { comentario: @facultad.comentario, nombre: @facultad.nombre, web: @facultad.web }
    assert_redirected_to facultad_path(assigns(:facultad))
  end

  test "should destroy facultad" do
    log_in_as(@admin)
    assert_difference('Facultad.count', -1) do
      delete :destroy, id: @facultad
    end

    assert_redirected_to facultads_path
  end
end
