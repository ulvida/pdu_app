require 'test_helper'

class SedesControllerTest < ActionController::TestCase
  setup do
    @sede = sedes(:one)
    @admin = usuarios(:one)
    @non_admin = usuarios(:two)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:sedes)
  end

  test "should get new" do
    log_in_as(@admin)
    get :new
    assert_response :success
  end

  test "should create sede when logged as admin" do
    log_in_as(@admin)
    assert_difference('Sede.count') do
      post :create, sede: { cenur_id: @sede.cenur_id, comentario: @sede.comentario, nombre: @sede.nombre, web: @sede.web }
    end

    assert_redirected_to sede_path(assigns(:sede))
  end

  test "should show sede" do
    get :show, id: @sede
    assert_response :success
  end

  test "should get edit" do
    log_in_as(@admin)
    get :edit, id: @sede
    assert_response :success
  end

  test "should update sede" do
    log_in_as(@admin)
    patch :update, id: @sede, sede: { cenur_id: @sede.cenur_id, comentario: @sede.comentario, nombre: @sede.nombre, web: @sede.web }
    assert_redirected_to sede_path(assigns(:sede))
  end

  test "should destroy sede" do
    log_in_as(@admin)
    assert_difference('Sede.count', -1) do
      delete :destroy, id: @sede
    end

    assert_redirected_to sedes_path
  end
end
