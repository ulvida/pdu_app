require 'test_helper'

class UsuarioTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @usuario = usuarios(:one)
    @usuario.password = @usuario.password_confirmation = "password"
  end

  test "should be valid" do
    assert @usuario.valid?
  end

  test "nombre should be present" do
    @usuario.nombre = "       "
    assert_not @usuario.valid?
  end

  test "email debe estar presente" do
    @usuario.email = "       "
    assert_not @usuario.valid?
  end

  test "name should not be too long" do
    @usuario.nombre = "a" * 51
    assert_not @usuario.valid?
  end

  test "email should not be too long" do
    @usuario.email = "a" * 244 + "@example.com"
    assert_not @usuario.valid?
  end

  test "email validation should accept valid addresses" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                         first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @usuario.email = valid_address
      assert @usuario.valid?, "#{valid_address.inspect} should be valid"
    end
  end

  test "email validation should reject invalid addresses" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @usuario.email = invalid_address
      assert_not @usuario.valid?, "#{invalid_address.inspect} should be invalid"
    end
  end

  test "email addresses should be unique" do
    duplicate_user = @usuario.dup
    @usuario.save
    assert_not duplicate_user.valid?
  end

  test "email addresses should be unique, case insensitive" do
    duplicate_user = @usuario.dup
    duplicate_user.email = @usuario.email.upcase
    @usuario.save
    assert_not duplicate_user.valid?
  end

  test "password should be present (nonblank)" do
    @usuario.password = @usuario.password_confirmation = " " * 6
    assert_not @usuario.valid?
  end

  test "password should have a minimum length" do
    @usuario.password = @usuario.password_confirmation = "a" * 5
    assert_not @usuario.valid?
  end
end
