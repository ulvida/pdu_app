class Usuario < ActiveRecord::Base
  has_many    :actividads
  belongs_to  :caracter
  belongs_to  :sede
  belongs_to  :facultad
  has_secure_password
  before_save { self.email = email.downcase }
  validates :nombre,  presence: true, length: { maximum: 50 }
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 255 },
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  validates :password, presence: true, length:   { minimum: 6 }, allow_nil: true
   # self.per_page = 3

  # Returns the hash digest of the given string.
  def Usuario.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
                                                  BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end
  # Nombre completo para los select
  def nombre_completo
    "#{nombre} #{apellido}"
  end
end
