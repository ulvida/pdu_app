class Actividad < ActiveRecord::Base
  belongs_to  :usuario
  belongs_to  :tipo
  belongs_to  :tipo_actividad, :foreign_key => 'actividad_id'
validates :tipo_id, presence: true
validates :actividad_id, presence: true
validates :usuario_id, presence: true
validates :nombre, presence: true
end
