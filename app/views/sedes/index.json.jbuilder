json.array!(@sedes) do |sede|
  json.extract! sede, :id, :nombre, :web, :cenur_id, :comentario
  json.url sede_url(sede, format: :json)
end
