json.array!(@usuarios) do |usuario|
  json.extract! usuario, :id, :nombre, :apellido, :ci, :grado, :sede_id, :facultad_id, :anio_aprobado, :horas_servicio, :horas_sede, :dt, :viajero, :caracter_id, :calidad, :art9
  json.url usuario_url(usuario, format: :json)
end
