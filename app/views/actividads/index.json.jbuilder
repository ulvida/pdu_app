json.array!(@actividads) do |actividad|
  json.extract! actividad, :id, :tipo_id, :actividad_id, :docente_id, :nombre, :titulo, :autores, :presentador, :participantes, :fecha, :lugar, :origen_financiamiento, :comentario
  json.url actividad_url(actividad, format: :json)
end
