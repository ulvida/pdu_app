json.array!(@facultads) do |facultad|
  json.extract! facultad, :id, :nombre, :web, :comentario
  json.url facultad_url(facultad, format: :json)
end
