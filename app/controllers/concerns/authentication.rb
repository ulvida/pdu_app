## http://elegantbrew.tumblr.com/post/70990048275/controller-concerns-in-rails-4
## codigo compartido entre controllers

module Authentication
  extend ActiveSupport::Concern

  # Confirms a logged-in user.
  def logged_in_user
    unless logged_in?
      store_location
      flash[:danger] = "Por favor, conéctate."
      redirect_to login_url
    end
  end

#  # Confirms the correct user: itself or superadmin
#  def correct_user
#    @usuario = Usuario.find(params[:id])
#    if !current_user.superadmin?
#      redirect_to(root_url) unless current_user?(@usuario)
#    end
#  end

  # Confirms an admin user.
  def admin_user
    redirect_to(root_url) unless current_user.superadmin?
  end
end


