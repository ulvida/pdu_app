class ActividadsController < ApplicationController
  include Authentication

  before_action :set_actividad, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:new, :create, :edit, :update, :destroy]
  before_action :usuario_actividad_o_admin,  only: [:edit, :update]

  # GET /usuarios/:usuario_id/actividads
  # GET /actividads
  # GET /actividads.json
  def index
    # Si hay usuario, de qué usuario vemos las actividades, sino todas
    @usuario = Usuario.find(params[:usuario_id]) if params[:usuario_id]
    @usuario ? @actividads = @usuario.actividads : @actividads = Actividad.all
  end

  # GET /actividads/1
  # GET /actividads/1.json
  def show
  end

  # GET /usuarios/:usuario_id/actividads/new
  # GET /actividads/new
  def new
    # Si hay usuario, de qué usuario vemos las actividades, sino todas
    @usuario = Usuario.find(params[:usuario_id]) if params[:usuario_id]
    @usuario ? @actividad = @usuario.actividads.build : @actividad = Actividad.new
  end

  # GET /actividads/1/edit
  def edit
  end

  # POST /actividads
  # POST /actividads.json
  def create
    @actividad = Actividad.new(actividad_params)

    respond_to do |format|
      if @actividad.save
        format.html { redirect_to @actividad, notice: 'Actividad was successfully created.' }
        format.json { render :show, status: :created, location: @actividad }
      else
        format.html { render :new }
        format.json { render json: @actividad.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /actividads/1
  # PATCH/PUT /actividads/1.json
  def update
    respond_to do |format|
      if @actividad.update(actividad_params)
        format.html { redirect_to @actividad, notice: 'Actividad was successfully updated.' }
        format.json { render :show, status: :ok, location: @actividad }
      else
        format.html { render :edit }
        format.json { render json: @actividad.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /actividads/1
  # DELETE /actividads/1.json
  def destroy
    @actividad.destroy
    respond_to do |format|
      format.html { redirect_to actividads_url, notice: 'Actividad was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_actividad
      @actividad = Actividad.find(params[:id])
    end

  # Confirms the correct user: itself or superadmin
  def usuario_actividad_o_admin
    if !current_user.superadmin?
      @actividad = Actividad.find(params[:id])
      @usuario = @actividad.usuario
      redirect_to(root_url) unless current_user?(@usuario)
    end
  end


    # Never trust parameters from the scary internet, only allow the white list through.
    def actividad_params
      params.require(:actividad).permit(:tipo_id, :actividad_id, :usuario_id, :nombre, :titulo, :autores, :presentador, :participantes, :fecha, :lugar, :origen_financiamiento, :comentario)
    end
end
