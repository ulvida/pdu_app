class CenursController < ApplicationController
  include Authentication

  before_action :set_cenur, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:new, :create, :edit, :update, :destroy]
  before_action :admin_user,     only: [:new, :create, :edit, :update, :destroy]

  # GET /cenurs
  # GET /cenurs.json
  def index
    @cenurs = Cenur.all
  end

  # GET /cenurs/1
  # GET /cenurs/1.json
  def show
  end

  # GET /cenurs/new
  def new
    @cenur = Cenur.new
  end

  # GET /cenurs/1/edit
  def edit
  end

  # POST /cenurs
  # POST /cenurs.json
  def create
    @cenur = Cenur.new(cenur_params)

    respond_to do |format|
      if @cenur.save
        format.html { redirect_to @cenur, notice: 'Cenur was successfully created.' }
        format.json { render :show, status: :created, location: @cenur }
      else
        format.html { render :new }
        format.json { render json: @cenur.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cenurs/1
  # PATCH/PUT /cenurs/1.json
  def update
    respond_to do |format|
      if @cenur.update(cenur_params)
        format.html { redirect_to @cenur, notice: 'Cenur was successfully updated.' }
        format.json { render :show, status: :ok, location: @cenur }
      else
        format.html { render :edit }
        format.json { render json: @cenur.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cenurs/1
  # DELETE /cenurs/1.json
  def destroy
    @cenur.destroy
    respond_to do |format|
      format.html { redirect_to cenurs_url, notice: 'Cenur was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cenur
      @cenur = Cenur.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cenur_params
      params.require(:cenur).permit(:nombre, :web, :comentario)
    end
end
