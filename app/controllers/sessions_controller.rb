class SessionsController < ApplicationController
  def new
  end
  def create
    usuario = Usuario.find_by(email: params[:session][:email].downcase)
    if usuario && usuario.authenticate(params[:session][:password])
      # Log the user in and redirect to the user's show page.
      log_in usuario
      redirect_back_or usuario

    else
      flash.now[:danger] = 'Invalid email/password combination' 
      #flash.now[:error] = 'Invalid email/password combination' # Not quite right!
      #redirect_to home_path # no anduvo...
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end
end
