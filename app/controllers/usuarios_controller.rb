class UsuariosController < ApplicationController

  include Authentication

  before_action :set_usuario, only: [:show, :edit, :update, :destroy]
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update]
  before_action :admin_user,     only: :destroy


  # GET /usuarios
  # GET /usuarios.json
  def index
    # @usuarios = Usuario.all
    @usuarios = Usuario.paginate(page: params[:page])

  end

  # GET /usuarios/1
  # GET /usuarios/1.json
  def show
  end

  # GET /usuarios/new
  def new
    @usuario = Usuario.new
  end

  # GET /usuarios/1/edit
  def edit
    @usuario = Usuario.find(params[:id])
  end

  # POST /usuarios
  # POST /usuarios.json
  def create
    @usuario = Usuario.new(usuario_params)

    respond_to do |format|
      if @usuario.save
	log_in @usuario
        flash[:success] = "¡Bienvenida a PDU App!"
        format.html { redirect_to @usuario, notice: 'Usuario was successfully created.' }
        format.json { render :show, status: :created, location: @usuario }
      else
        format.html { render :new }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /usuarios/1
  # PATCH/PUT /usuarios/1.json
  def update
    respond_to do |format|
      if @usuario.update(usuario_params)
        flash[:success] = "Perfil actualizado"
        format.html { redirect_to @usuario, notice: 'Usuario was successfully updated.' }
        format.json { render :show, status: :ok, location: @usuario }
      else
        format.html { render :edit }
        format.json { render json: @usuario.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /usuarios/1
  # DELETE /usuarios/1.json
  def destroy
    @usuario.destroy
    flash[:success] = "Usuario borrado"
    respond_to do |format|
      format.html { redirect_to usuarios_url, notice: 'Usuario was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    # Confirms a logged-in user.
#    def logged_in_user
#      unless logged_in?
#        store_location
#        flash[:danger] = "Por favor, conéctate."
#        redirect_to login_url
#      end
#    end
#
    # Confirms the correct user: itself or superadmin
    def correct_user
      if !current_user.superadmin?
        @usuario = Usuario.find(params[:id])
        redirect_to(root_url) unless current_user?(@usuario)
      end
    end
#
#    # Confirms an admin user.
#    def admin_user
#      redirect_to(root_url) unless current_user.superadmin?
#    end

    def set_usuario
      @usuario = Usuario.find(params[:id])
      @caracters = Caracter.all
      @sedes = Sede.all
      @facultads = Facultad.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def usuario_params
      params.require(:usuario).permit(:nombre, :apellido, :password, :password_confirmation, :email, :ci, :grado, :sede_id, :facultad_id, :anio_aprobado, :horas_servicio, :horas_sede, :dt, :viajero, :caracter_id, :calidad, :art9)
    end
end
